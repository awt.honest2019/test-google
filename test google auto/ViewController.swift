//
//  ViewController.swift
//  test google auto
//
//  Created by mac on 22/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import GooglePlaces
import MaterialComponents.MaterialTextFields
import SDWebImage
import IQKeyboardManagerSwift

struct Key {
    static let MapAPIKey = ""
}

struct AppColor {
    static let appNavBackColor: UIColor = #colorLiteral(red: 0.737254902, green: 0, blue: 0, alpha: 1)
    static let appHeaderColor: UIColor = #colorLiteral(red: 0.5568627451, green: 0, blue: 0, alpha: 1)
    static let appNavBarColor: UIColor = #colorLiteral(red: 0.9137254902, green: 0.2941176471, blue: 0.2078431373, alpha: 1)
    static let appColor : UIColor = #colorLiteral(red: 0.7764705882, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
    static let appColor2 : UIColor = #colorLiteral(red: 0.831372549, green: 0.1960784314, blue: 0.1294117647, alpha: 1)
}

let kDefaultAnimationDuration = 0.33

class ViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: outlets and variables
    var placeObj: GMSPlace!
    
    var imgPlace: UIImage!
    var imgMap: UIImage!
    var placeStr: String = ""
    var channelArr: [String] = []
    var areaStrArr: [String] = []
    var datachannelArr: [String] = []
    var dataareaStrArr: [String] = []
    
    var newframe: CGRect!
    var placeAddressStr: String = ""
    var backStr: String = ""
    var imageLocation :CLLocationCoordinate2D?
    var searchResults: [String] = []
    var searchTxt = ""
    var newFrame: CGRect!
    var tblStr: String = ""
    var autoCompleteCharacterCount = 0
    var timer = Timer()
    var checkAreaStr: String = ""
    var isChoosingAgain: Bool = false
    var isChangeTap: Bool = false
    var placeReviewCounts: String = ""
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        if placeObj != nil {
            let someString = placeObj.website != nil ? (placeObj.website?.absoluteString)! : ""
            var stringWithoutHttp = someString.replacingOccurrences(of: "http://www.", with: "")
            
            for _ in 1...10 {
                if (stringWithoutHttp.contains("https:") || stringWithoutHttp.contains("www") || stringWithoutHttp.contains("http://www.") || stringWithoutHttp.contains("http://")) {
                    stringWithoutHttp = stringWithoutHttp.replacingOccurrences(of: "https:", with: "")
                    stringWithoutHttp = stringWithoutHttp.replacingOccurrences(of: "www.", with: "")
                    stringWithoutHttp = someString.replacingOccurrences(of: "http://www.", with: "")
                    stringWithoutHttp = someString.replacingOccurrences(of: "http://", with: "")
                }
                else {
                    break
                }
            }
        }
      
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
      
        
        setupNavigationBar()
           
        
            showPlacePicker()
        
        
     
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
              let statusBarHeight: CGFloat = app.statusBarFrame.size.height
              
              let statusbarView = UIView()
              statusbarView.backgroundColor = AppColor.appHeaderColor
              view.addSubview(statusbarView)
            
              statusbarView.translatesAutoresizingMaskIntoConstraints = false
              statusbarView.heightAnchor
                  .constraint(equalToConstant: statusBarHeight).isActive = true
              statusbarView.widthAnchor
                  .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
              statusbarView.topAnchor
                  .constraint(equalTo: view.topAnchor).isActive = true
              statusbarView.centerXAnchor
                  .constraint(equalTo: view.centerXAnchor).isActive = true
        } else {
            let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
                statusBar.backgroundColor = AppColor.appHeaderColor
            }
        }
        navigationController!.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationItem.backBarButtonItem?.isEnabled = false;
        self.navigationController?.navigationBar.barStyle = .black
    
    }
    
    func showPlacePicker() {
       
        
        let placePicker = self.storyboard?.instantiateViewController(withIdentifier: "PlacePickerController") as! PlacePickerController

        if imageLocation != nil {
            placePicker.currentLocation = imageLocation
        }
        placePicker.delegate = self
        
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = UIColor(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.backgroundColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0) ]
        
        
        UINavigationBar.appearance().isTranslucent = true
        self.navigationController?.navigationBar.isTranslucent = true
            
        navigationController?.navigationBar.tintColor = UIColor(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.backgroundColor = .white
        placePicker.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:  UIColor(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0) ]
        
        self.present(placePicker, animated: true, completion: nil)
    }
    //MARK:- Helper Functions END
    
    //MARK:- Setup navigation bar
    func setupNavigationBar () {
       
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.barTintColor = AppColor.appColor
        
       
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
              let statusBarHeight: CGFloat = app.statusBarFrame.size.height
              
              let statusbarView = UIView()
              statusbarView.backgroundColor = AppColor.appHeaderColor
              view.addSubview(statusbarView)
            
              statusbarView.translatesAutoresizingMaskIntoConstraints = false
              statusbarView.heightAnchor
                  .constraint(equalToConstant: statusBarHeight).isActive = true
              statusbarView.widthAnchor
                  .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
              statusbarView.topAnchor
                  .constraint(equalTo: view.topAnchor).isActive = true
              statusbarView.centerXAnchor
                  .constraint(equalTo: view.centerXAnchor).isActive = true
        } else {
            let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
                statusBar.backgroundColor = AppColor.appHeaderColor
            }
        }
        navigationController!.setNeedsStatusBarAppearanceUpdate()
        
        UINavigationBar.appearance().barTintColor = AppColor.appNavBackColor // Background color
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
                
}

// MARK: - PickerViewControllerDelegate
extension ViewController: PlacePickerControllerDelegate {
    func placePickerController(controller: PlacePickerController, didSelectPlace place: GMSPlace) {
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
              let statusBarHeight: CGFloat = app.statusBarFrame.size.height
              
              let statusbarView = UIView()
              statusbarView.backgroundColor = AppColor.appHeaderColor
              view.addSubview(statusbarView)
            
              statusbarView.translatesAutoresizingMaskIntoConstraints = false
              statusbarView.heightAnchor
                  .constraint(equalToConstant: statusBarHeight).isActive = true
              statusbarView.widthAnchor
                  .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
              statusbarView.topAnchor
                  .constraint(equalTo: view.topAnchor).isActive = true
              statusbarView.centerXAnchor
                  .constraint(equalTo: view.centerXAnchor).isActive = true
        } else {
            let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
                statusBar.backgroundColor = AppColor.appHeaderColor
            }
        }
        
        controller.dismiss(animated: true, completion: nil)
        
       
        
    }
    
    func placePickerControllerDidCancel(controller: PlacePickerController) {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
              let statusBarHeight: CGFloat = app.statusBarFrame.size.height
              
              let statusbarView = UIView()
              statusbarView.backgroundColor = AppColor.appHeaderColor
              view.addSubview(statusbarView)
            
              statusbarView.translatesAutoresizingMaskIntoConstraints = false
              statusbarView.heightAnchor
                  .constraint(equalToConstant: statusBarHeight).isActive = true
              statusbarView.widthAnchor
                  .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
              statusbarView.topAnchor
                  .constraint(equalTo: view.topAnchor).isActive = true
              statusbarView.centerXAnchor
                  .constraint(equalTo: view.centerXAnchor).isActive = true
        } else {
            let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
            if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
                statusBar.backgroundColor = AppColor.appHeaderColor
            }
        }
        navigationController!.setNeedsStatusBarAppearanceUpdate()

        controller.dismiss(animated: true, completion: nil)
       
    }
}





