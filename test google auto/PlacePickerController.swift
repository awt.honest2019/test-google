//
//  PlacePickerController.swift
//  test google auto
//
//  Created by mac on 22/01/20.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation
import MaterialComponents
import SDWebImage

protocol PlacePickerControllerDelegate: class {
    func placePickerController(controller: PlacePickerController, didSelectPlace place: GMSPlace)
    func placePickerControllerDidCancel(controller: PlacePickerController)
}

class PlacePickerController: UIViewController {
    
    public unowned var delegate: PlacePickerControllerDelegate?
    @IBOutlet weak var placesTable: UITableView!
    @IBOutlet weak var mapVw: GMSMapView!
    @IBOutlet weak var searchBtn : MDCFlatButton!
    @IBOutlet weak var cancelBtn : MDCFlatButton!
    
    var currentLocation: CLLocationCoordinate2D!
    var changeLocationTap: Bool = false
    var locManager = CLLocationManager()
    var placesArr = NSArray()
    var token = GMSAutocompleteSessionToken()
    var isMapReady : Bool = false
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        token = GMSAutocompleteSessionToken.init()
        mapVw.delegate = self
        locManager.delegate = self
        if currentLocation != nil {
            nearbyLocations(latitude: currentLocation.latitude,longitude: currentLocation.longitude)
            let camera = GMSCameraPosition.camera(withLatitude: currentLocation.latitude, longitude: currentLocation.longitude, zoom: 16)
            mapVw?.camera = camera
            mapVw?.animate(to: camera)
        }
        else {
           setupLocationManager()
        }
        setNeedsStatusBarAppearanceUpdate()
        searchBtn.inkMaxRippleRadius = searchBtn.frame.width/2
        searchBtn.backgroundColor = .clear
        searchBtn.inkStyle = .unbounded
               
        mapVw.isMyLocationEnabled = true
        mapVw.settings.myLocationButton = true

        cancelBtn.setTitleColor(.white, for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isMapReady = true
    }
    
    func nearbyLocations(latitude: Double, longitude: Double) {
        
        let jsonUrlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude),\(longitude)&rankby=distance&key=AIzaSyAlr-i71H5Xb9XYNsvURbUS4OXhknTsUow" // taxi app key

        guard let url = URL(string: jsonUrlString) else { return }

        URLSession.shared.dataTask(with: url) { (data, respone, err) in

            guard let data = data else { return }

            do {
                guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else { return }
                print(json)
                if "\(json["status"] ?? "Fail")" == "OK" {
                    self.placesArr = json["results"] as! NSArray
                    DispatchQueue.main.async {
                        self.mapVw.clear()
                        self.placesTable.reloadData()
                        for data in self.placesArr {
                            let placeDetail = data as! NSDictionary
                            let geometryDic = placeDetail["geometry"] as! NSDictionary
                            let locDic = geometryDic["location"] as! NSDictionary
                            
                            let location = CLLocationCoordinate2D(latitude:  Double("\(locDic["latitude"] ?? 3.000)")! , longitude: Double("\(locDic["longitude"] ?? 4.000)")!)
                            let marker = GMSMarker()
                            marker.position = location
                            marker.icon = UIImage(named: "splashlogo2")
                            marker.map = self.mapVw
                        }
                    }
                }
            } catch let jsonErr {
                print("json error:", jsonErr)
            }

        }.resume()
    }
    
    func setupLocationManager() {
        locManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locManager.allowsBackgroundLocationUpdates = false
        locManager.startUpdatingLocation()
    }
    
    @IBAction private func cancelSelection() {
        self.delegate?.placePickerControllerDidCancel(controller: self)
    }
       
    @IBAction private func showPlacesSearch() {
        
        let uibar = UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self])
        
        uibar.defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        uibar.backgroundColor = .white
        
        let main_string = "Search"
        let range = (main_string as NSString).range(of: main_string)
        let attributedString = NSMutableAttributedString(string:main_string)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white , range: range)
        uibar.attributedPlaceholder = attributedString
                
        let bar = UISearchBar.appearance(whenContainedInInstancesOf: [UINavigationBar.self])
        bar.setImage(UIImage(named: "search_white_18pt"), for: .search, state: .normal)
                
        UINavigationBar.appearance().barTintColor = AppColor.appNavBackColor // Background color
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        UINavigationBar.appearance().barTintColor = AppColor.appHeaderColor
        UINavigationBar.appearance().tintColor = .white
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = AppColor.appHeaderColor
        navigationController?.navigationBar.backgroundColor = AppColor.appHeaderColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        
        UINavigationBar.appearance().isTranslucent = true
        self.navigationController?.navigationBar.isTranslucent = true
                
        navigationController?.navigationBar.tintColor = UIColor(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0)
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.backgroundColor = .white
        autocompleteController.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:  UIColor(red: 60.0/255.0, green: 60.0/255.0, blue: 60.0/255.0, alpha: 1.0) ]
        
        present(autocompleteController, animated: true, completion: nil)
    }
}

// MARK: - GMSAutocompleteViewControllerDelegate
extension PlacePickerController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        delegate?.placePickerController(controller: self, didSelectPlace: place)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension PlacePickerController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        placesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PlacesCell = placesTable.dequeueReusableCell(withIdentifier: "PlacesCell") as! PlacesCell
        
        let placeDetail = placesArr[indexPath.row] as! NSDictionary
        cell.placeAddLbl.text = "\(placeDetail["vicinity"] ?? "ds")"
        cell.placeNameLbl.text = "\(placeDetail["name"] ?? "ds")"
        cell.iconIV.sd_setImage(with: URL(string: "\(placeDetail["icon"] ?? "ds")"), placeholderImage: #imageLiteral(resourceName: "white"), options: SDWebImageOptions(rawValue: 0), completed: { (image, error, cacheType, imageURL) in
           cell.iconIV.image = image
        })
        
        let categoryOfPlace = placeDetail["types"] as! NSArray
        
        for (i, item) in categoryOfPlace.enumerated() {
            let str = item as! String
            if i == 0 {
                cell.categoryLbl.text = str
            }
            else {
                cell.categoryLbl.text = cell.categoryLbl.text! + ", " + str
            }
        }
        
        /*
        {
            icon = "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png";
            id = 32e9e1c49b9b93e4cdec6dc1e55cc342dc970314;
            name = "Singapore Holiday Package";
            "place_id" = "ChIJ3f_9XK79YjkR2ybXg7HSTbs";
            "user_ratings_total" = 2;
            vicinity = "26/2, Leela Building 1st Floor, New Palasia, Indore";
            rating = 5;
            reference = "ChIJ3f_9XK79YjkR2ybXg7HSTbs";
            scope = GOOGLE;
            
            "opening_hours" =     {
                "open_now" = 1;
            };
            geometry =     {
                location =         {
                    lat = "22.7293742";
                    lng = "75.8855312";
                };
                viewport =         {
                    northeast =             {
                        lat = "22.7307044802915";
                        lng = "75.88682573029152";
                    };
                    southwest =             {
                        lat = "22.72800651970849";
                        lng = "75.8841277697085";
                    };
                };
            };
            photos =     (
                        {
                    height = 620;
                    "html_attributions" =             (
                        "<a href=\"https://maps.google.com/maps/contrib/117087499145661834861\">Singapore Holiday Package</a>"
                    );
                    "photo_reference" = "CmRaAAAA0DE8fBk8ILv-cAzws9Y6P2FH_wfDlivEf88Pki7eOJgyzZSO0QEsHBkokcvgxgrG4Pv-ONoiMFGwq2pCoUNjWy7PR41BhITRkwm-xaTYPC9GGZsix5tGDQsx7swAchkGEhBA4WquNXM3nemvbjy1eQNTGhTsv8Bqg_Mh_3iTM2JlJj9NtnenkQ";
                    width = 1600;
                }
            );
            
            "plus_code" =     {
                "compound_code" = "PVHP+P6 New Palasia, Indore, Madhya Pradesh, India";
                "global_code" = "7JJQPVHP+P6";
            };
            
            types =     (
                "travel_agency",
                "point_of_interest",
                establishment
            );
            
        }*/
               
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let placeDetail = placesArr[indexPath.row] as! NSDictionary
        let gm =  GMSPlacesClient()
        gm.fetchPlace(fromPlaceID: "\(placeDetail["place_id"] ?? "ds")", placeFields: GMSPlaceField.all, sessionToken: token) { (place, err) in
            if err == nil {
                self.delegate?.placePickerController(controller: self, didSelectPlace: place!)
            }
            else {
                print(err?.localizedDescription ?? "fd")
            }
        }
    }
}


// MARK: - CLLocationManagerDelegate
extension PlacePickerController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) -> Void{
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            self.setupLocationManager()
        case .denied, .restricted:
            locManager.requestWhenInUseAuthorization()
            print("denied")
        default:
            print("location default case")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.last?.coordinate
        nearbyLocations(latitude: currentLocation.latitude,longitude: currentLocation.longitude)
        let camera = GMSCameraPosition.camera(withLatitude: currentLocation.latitude, longitude: currentLocation.longitude, zoom: 16)
        mapVw?.camera = camera
        mapVw?.animate(to: camera)
        locManager.stopUpdatingLocation()
    }
}

// MARK: - GMSMapViewDelegate
extension PlacePickerController: GMSMapViewDelegate {
    @objc(mapView:didTapMarker:) func mapView(_: GMSMapView, didTap marker: GMSMarker) -> Bool {
        print("You tapped : \(marker.position.latitude),\(marker.title ?? "https://lovethis.place")")
        return true
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if isMapReady == true {
            nearbyLocations(latitude: position.target.latitude, longitude: position.target.longitude)
        }
    }
}

//MARK: PlacesCell Class
class PlacesCell: UITableViewCell {
    @IBOutlet weak var placeNameLbl: UILabel!
    @IBOutlet weak var placeAddLbl: UILabel!
    @IBOutlet weak var iconIV: UIImageView!
    @IBOutlet weak var categoryLbl: UILabel!
}
